var YORA_API_URL = 'https://api.cipherzero.duckdns.org/';
var ADMIN_TOKEN_EXPIRY = 24*60*60;
var caseID = 1;
var evidenceID = 0;

function getCaseURL() {
	valid = validateCase();
	
	if (valid == true) {
		caseID = document.getElementById("caseNo").value;
	window.location.href = "caseDemo.html?cid=" + caseID;	
	}
}

function getCaseID() {
	var str = window.location.href;
	var reg = "/<?\d+/";
	var obj = /<?\d+/.exec(str);
	caseID = obj[0];
	apiCallGET("case", [["cid",caseID],], fillCase);
}

function getEvidenceURL(eid) {
    window.location.href = "evidenceDemo.html?eid=" + eid;
}

function getEvidenceID() {
	var str = window.location.href;
	var reg = "/<?\d+/";
	var obj = /<?\d+/.exec(str);
	evidenceID = obj[0];
	apiCallGET("evidence", [["eid",evidenceID],], fillEvidence);
}

function getCookie (cname) {
  var name = cname + '='
  var decodedCookie = decodeURIComponent(document.cookie)
  var ca = decodedCookie.split(';')
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ''
}


function setCookie(cname, cvalue, expiry)
{
  var d = new Date();
  d.setTime(d.getTime() + expiry*1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/;SameSite=None; Secure";
}


// DEFAULT FUNCTIONS
function callback(response)
{
	//do stuff with response inside this function
	console.log(response);
}

function fillCase(response) {
	document.getElementById("caseTitle").innerHTML = "Viewing Case #00000" + response.cid;
	document.getElementById("createdBy").value = response.associated_entity;
	document.getElementById("dateCreated").value = response.date_created;
	if (response.active == 1) {
		document.getElementById("caseStatus").value = "Active";
	}
	else {
		document.getElementById("caseStatus").value = "Closed";
	}
	
	document.getElementById("caseNotes").value = response.note;
	
	var evidenceLinks = response.evidence.length + 0;
	for (i = 0; i < evidenceLinks; i++) {
	  var div = document.createElement("div");   // Create a <button> element
	  document.getElementById("contentContainer").appendChild(div);               // Append <button> to <body>
	  div.classList.add("evidence");
	  div.id = "evidence" + i;
	  
	  if (response.evidence[i].has_links > 0) {
		  div.style.backgroundColor = "#ff8d85";
	  }
	
	  var p = document.createElement("p");
	  document.getElementById("evidence" + i).appendChild(p);
	  p.classList.add("evidenceTitle");
	  p.innerHTML = response.evidence[i].name;
	
	  var btn = document.createElement("button");
	  btn.innerHTML = ">";
	  document.getElementById("evidence" + i).appendChild(btn);
	  btn.classList.add("eLink");
	  btn.id = "evidence" + i + "btn";
	  document.getElementById("evidence" + i + "btn").setAttribute('onclick','getEvidenceURL(' + response.evidence[i].eid + ')');
	}
	var btn = document.createElement("button");
	btn.innerHTML = "Add Case"
	document.getElementById("contentContainer").appendChild(btn);
	btn.id = "addCase"
	
}

function fillEvidence(response) {
	document.getElementById("evidenceTitle").innerHTML = "Viewing Evidence #00000" + response.eid;
	document.getElementById("evidence").value = response.evidence;
	document.getElementById("dateAdded").value = response.date_added;
	document.getElementById("eType").value = response.evidence_type;
	document.getElementById("currency").value = response.currency;
	document.getElementById("origin").value = response.origin;
	document.getElementById("entity").value = response.associated_entity;
	document.getElementById("eType").value = response.evidence_type;
	document.getElementById("caseNotes").value = response.note;
	//document.getElementById("evidenceName").innerHTML = response.name;
	//document.getElementById("evidenceOrigin").innerHTML = response.origin;
	//document.getElementById("evidenceEntity").innerHTML = response.associated_entity;
	
	var links = response.links.length;
	  //table generation
	  var table = document.createElement("table");
	  document.getElementById("evidenceContainer").appendChild(table);
	  table.id = "evidenceTableJava";
	  
	  if (response.links.length == 0){
	      var row = table.insertRow(0);
		  row.classList.add("evidenceRow");
		  
		  var cell1 = row.insertCell(0);
		  cell1.classList.add("evidenceColumn");
		  var cell2 = row.insertCell(0);
		  cell2.classList.add("evidenceColumn");
		  var cell3 = row.insertCell(0);
		  cell3.classList.add("evidenceColumn");
		  var cell4 = row.insertCell(0);
		  cell4.classList.add("evidenceColumn");
		  
		  cell1.innerHTML = "";
		  cell2.innerHTML = "N/A";
		  cell3.innerHTML = "N/A";
		  cell4.innerHTML = "N/A"; 
	  } else {
	  //start for loop here
		  for (i=0; i<links; i++){
			  var row = table.insertRow(0);
			  row.classList.add("evidenceRow");
			  
			  var cell1 = row.insertCell(0);
			  cell1.classList.add("evidenceColumn");
			  var cell2 = row.insertCell(0);
			  cell2.classList.add("evidenceColumn");
			  var cell3 = row.insertCell(0);
			  cell3.classList.add("evidenceColumn");
			  var cell4 = row.insertCell(0);
			  cell4.classList.add("evidenceColumn");
			  
			  cell1.innerHTML = "";
			  cell1.id = "btn";
			  var btn = document.createElement("button");
			  document.getElementById("btn").appendChild(btn);
			  btn.innerHTML = ">";
			  document.getElementById("btn").setAttribute('onclick','getEvidenceURL(' + response.links[i].eid + ')');
			  
			  
			  cell2.innerHTML = response.links[i].associated_entity;
			  cell3.innerHTML = response.links[i].origin;
			  cell4.innerHTML = response.links[i].name;  
		  }
	  }
	  
	  //header generation
	  var row = table.insertRow(0);
	  row.classList.add("evidenceRow");
	  
	  var cell1 = row.insertCell(0);
	  cell1.classList.add("evidenceColumn");
	  var cell2 = row.insertCell(0);
	  cell2.classList.add("evidenceColumn");
	  var cell3 = row.insertCell(0);
	  cell3.classList.add("evidenceColumn");
	  var cell4 = row.insertCell(0);
	  cell4.classList.add("evidenceColumn");
	  
	  cell1.innerHTML = "";
	  cell2.innerHTML = "Entity";
	  cell3.innerHTML = "Origin";
	  cell4.innerHTML = "Name";  
	
	  /*var tr = document.createElement("tr");   // Create a <button> element
	  document.getElementById("endOfHeader").appendChild(tr);               // Append <button> to <body>
	  tr.classList.add("evidenceRow");
	  tr.id = "evidenceRow1";
	  
	  var td = document.createElement("tr");
	  document.getElementById("evidenceRow1");
	  td.classList.add("evidenceColumn");
	  tr.id = "evidenceName1";
	  document.getElementById("evidenceName1").innerHTML = response.name;
	*/
}

function onClientHTTPError(code)
{
  console.log('Client recieved a bad HTTP response code: ' + code);
}

function onClientConnectionError()
{
  console.log('Client could not connect to the server');
}
//


/*
This function performs a HTTP request on a particular URL and endpoint.

This should not be called in a general context and should be called from another
function that uses it.
*/
function runRequest(url, endpoint, body, callback, method)
{
	var request = new XMLHttpRequest();
	var token = getCookie('token');

  console.log('Connecting to ' + url + endpoint);
  console.log('Token is ' + token);

	request.onload = function()
	{
	  if (this.status >= 200 && this.status < 400)
	  {
	  	let parsedResp = JSON.parse(this.response);

      console.log('Successfully connected and received a response')
      console.log(parsedResp)

	    callback(parsedResp);
	  }

	  else
	   {
	    // We reached our target server, but it returned an error
	    onClientHTTPError(this.status);
	   }
	};

	request.onerror = onClientConnectionError;

  request.open(method, url + endpoint, true);
	request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  // Only if a body has been provided, add the token to the data
  if (body)
  {
    if (token && endpoint != 'login')
  	{
  		body['token'] = token;
  	}
    request.send(JSON.stringify(body));
  }
	else
  {
    request.send();
  }
}


/*
runRequest wrapper for API GET requests.

args must be a list of (arg name, value) pairs.
*/
function apiCallGET(endpoint, args, callback)
{
  // Append the url arguments to the endpoint
  for (let i = 0; i < args.length; i++)
  {
    endpoint = endpoint + "?" + args[i][0] + '=' + args[i][1];

    if (i != args.length - 1)
    {
      endpoint = endpoint + '&';
    }
  }

  runRequest(YORA_API_URL, endpoint, null, callback, 'GET');
}

/*
runRequest wrapper for API POST requests.
*/
function apiCallPOST(endpoint, data, callback)
{
  runRequest(YORA_API_URL, endpoint, data, callback, 'POST');
}

function validateCase() {
  var input = document.getElementById("caseNo").value;
    if (input == "") {
    alert("Case must be filled out");
    return false;
  } else {
    if (input != 1) {
	alert("Case ID " + input + " not found. Please try again.")
	return false; 
  } else {
	  return true;
  }
  }
}
