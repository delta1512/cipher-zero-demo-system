import mysql.connector as cnx
from flask import g

import config as c


current_db = None


### ENGINE FUNCTIONS
def get_db():
    db = g.get('db', None)
    if hasattr(db, 'is_connected'):
        if db.is_connected():
            return db
        else:
            db.close()
            db = None
    if db is None:
        db = g.db = cnx.connect(
            host=c.db_host,
            user=c.db_user,
            passwd=c.db_pass,
            db=c.db_name
        )
    return db


def q_ro(query, args=(), one=False, alt_db=None):
    if not current_db is None and alt_db is None:
        db = current_db
    elif alt_db is None:
        db = get_db()
    else:
        db = alt_db

    cur = db.cursor()
    cur.execute(query, args)
    result = cur.fetchall()
    cur.close()
    return (result[0] if result else []) if one else result


def q_wo(query, args=(), alt_db=None):
    if not current_db is None and alt_db is None:
        db = current_db
    elif alt_db is None:
        db = get_db()
    else:
        db = alt_db

    cur = db.cursor()
    cur.execute(query, args)
    cur.close()
    db.commit()
###


### DICT CONVERSIONS
def case_to_dict(case: list):
    return {
        'cid' :                 str(case[0]),
        'associated_entity' :   str(case[1]),
        'date_created' :        str(case[2]),
        'active' :              str(case[3]),
        'note' :                str(case[4])
    }


def evidence_to_dict(evidence: list):
    return {
        'eid' :                 str(evidence[0]),
        'name' :                str(evidence[1]),
        'evidence' :            str(evidence[2]),
        'date_added' :          str(evidence[3]),
        'evidence_type' :       str(evidence[4]),
        'currency' :            str(evidence[5]),
        'origin' :              str(evidence[6]),
        'associated_case' :     str(evidence[7]),
        'note' :                str(evidence[8]),
        'associated_entity' :   str(evidence[9]) if len(evidence) == 10 else str(evidence[10])
    }


def evidence_to_list(evidence: list):
    final = []
    for e in evidence:
        final.append(evidence_to_dict(e))
    return final


def case_evidence_to_dict(evidence: list):
    return {
        'eid' :                 str(evidence[0]),
        'name' :                str(evidence[1]),
        'has_links' :           int(evidence[11])
    }


def case_evidence_to_list(evidence: list):
    final = []
    for e in evidence:
        final.append(case_evidence_to_dict(e))
    return final
###


def get_case(cid: str):
    case = q_ro(
        '''SELECT c.cid, e.name, c.date_created, c.active, c.note FROM inv_case c
        JOIN entity e ON c.associated_entity=e.entity_id
        WHERE c.cid=%s;''',
        (cid,),
        one=True
    )

    if not bool(case):
        return None, 404

    case = case_to_dict(case)

    evidence = q_ro(
        '''SELECT ev.*, e.name FROM evidence ev
        JOIN entity e ON ev.associated_entity=e.entity_id
        WHERE ev.associated_case=%s;''',
        (cid,)
    )

    # Search each evidence for links
    for i in range(len(evidence)):
        evidence[i] = list(evidence[i])
        eid = evidence[i][0]
        links = q_ro(
            'SELECT count(*) FROM evidence_link WHERE founder_eid=%s OR other_eid=%s;',
            (eid, eid),
            one=True
        )

        evidence[i].append(links[0] > 0)

    # Have to get links for each piece of evidence

    evidence = case_evidence_to_list(evidence)
    case['evidence'] = evidence

    return case, 200


def get_evidence_sql(eid: str):
    return q_ro(
        '''SELECT ev.*, e.name FROM evidence ev
        JOIN entity e ON ev.associated_entity=e.entity_id
        WHERE ev.eid=%s;''',
        (eid,),
        one=True
    )


def get_evidence(eid: str):
    # Start with the basic information
    evidence = get_evidence_sql(eid)

    if not bool(evidence):
        return None, 404

    evidence = evidence_to_dict(evidence)

    links = q_ro(
        '''SELECT * FROM evidence_link WHERE founder_eid=%s OR other_eid=%s;''',
        (eid, eid)
    )

    evidence_links = []

    for l in links:
        if l[2] == int(eid):
            leid = l[3]
        else:
            leid = l[2]

        ev = get_evidence_sql(leid)

        evidence_links.append({
            'eid' :                 str(ev[0]),
            'name' :                str(ev[1]),
            'origin' :              str(ev[6]),
            'associated_entity' :   str(ev[10])
        })

    evidence['links'] = evidence_links

    return evidence, 200
