import yaml
from flask import Flask, request, g, jsonify
from flask_cors import CORS

import backend


app = Flask(__name__)
CORS(app)


### BASIC FUNCTIONS
@app.teardown_appcontext
def close_connection(exception):
    if g.get('db', None) is not None and hasattr(g.get('db', None), 'close'):
        g.db.close()


def make_json_resp(data=None, code=200):
    if data is None:
        data = dict()

    assert isinstance(data, dict)

    if code == 200:
        return  jsonify(data)
    else:
        return ('Something went wrong (HTTP {} ERROR)'.format(code), code)
###


@app.route('/', methods=['GET'])
def api():
    return 'CRYPTO API is Online!'


@app.route('/openapi', methods=['GET'])
def openapi():
    with open('api.yaml', 'r') as f:
        documentation = f.read()
    return jsonify(yaml.safe_load(documentation))


@app.route('/case', methods=['GET'])
def case():
    cid = request.args.to_dict().get('cid', None)
    result, success = backend.get_case(cid)
    return make_json_resp(data=result, code=success)


@app.route('/evidence', methods=['GET'])
def evidence():
    eid = request.args.to_dict().get('eid', None)
    result, success = backend.get_evidence(eid)
    return make_json_resp(data=result, code=success)
