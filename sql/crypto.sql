CREATE DATABASE IF NOT EXISTS crypto_demo;

USE crypto_demo;


CREATE TABLE IF NOT EXISTS entity (
  entity_id             INT UNSIGNED AUTO_INCREMENT,
  name                  CHAR(64),

  PRIMARY KEY (entity_id)
);

CREATE TABLE IF NOT EXISTS inv_case (
  cid                   BIGINT UNSIGNED AUTO_INCREMENT,
  associated_entity     INT UNSIGNED,
  date_created          CHAR(10),
  active                BOOLEAN DEFAULT 1 NOT NULL,
  note                  TEXT,

  PRIMARY KEY (cid),
  FOREIGN KEY (associated_entity) REFERENCES entity (entity_id)
);

CREATE TABLE IF NOT EXISTS evidence (
  eid                   BIGINT UNSIGNED AUTO_INCREMENT,
  name                  CHAR(64),
  evidence              TEXT,
  date_added            CHAR(10),
  evidence_type         CHAR(64),
  currency              CHAR(6),
  origin                CHAR(64),
  associated_case       BIGINT UNSIGNED,
  note                  TEXT,
  associated_entity     INT UNSIGNED,

  PRIMARY KEY (eid),
  FOREIGN KEY (associated_entity) REFERENCES entity (entity_id)
);

CREATE TABLE IF NOT EXISTS evidence_link (
  elid                  BIGINT UNSIGNED AUTO_INCREMENT,
  time_found            BIGINT UNSIGNED,
  founder_eid           BIGINT UNSIGNED,
  other_eid             BIGINT UNSIGNED,

  PRIMARY KEY (elid)
);
